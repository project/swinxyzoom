-- SUMMARY --

SwinxyZoom is a image zoom jQuery plugin that can be applied to any image field.

Designed to work across a large range of designs, screens (mobile, tablets,
laptops) and devices from the latest UltraBooks to memory constrained mobile
devices with SwinxyZoom you can be sure the largest number of people will be
able to see your imagery the way it was intended.

Visit http://www.swinxyapps.com/pages/apps/swinxy-zoom/index.html for the more
details and to download the required jQuery library.

-- REQUIREMENTS --

* image
* libraries (2.x)

-- INSTALLATION --

1. Download and unpack the Libraries module directory in your modules folder
   (this will usually be "sites/all/modules/").

   Link: http://drupal.org/project/libraries

2. Download and unpack the SwinxyZoom module directory in your modules folder
   (this will usually be "sites/all/modules/").

3. Download and unpack the SwinxyZoom plugin in "sites/all/libraries".

   Make sure the path to the plugin file becomes:
   "sites/all/libraries/swinxyzoom/dist/*"
   "sites/all/libraries/swinxyzoom/libs/jquery.mousewheel.js"

   Link: http://www.swinxyapps.com/pages/apps/swinxy-zoom/index.html

4. Enable both Libraries and SwinxyZoom modules.

   More information about install Drupal modules can be found here:
   Link: https://drupal.org/documentation/install/modules-themes/modules-7

-- CONFIGURATION --

* Go to your field display settings page and select a "Swinxy Zoom" format.

* Optionally configure formatter using the formatter settings found using the
  cog on the right of the Format select list.

-- CONTACT --

JQuery Plugin:

* ShadowShade - http://www.swinxyapps.com/index.html

Module maintainers:

* Alan Davison (Alan D.) - http://drupal.org/user/198838

This project has been sponsored by:

* Glo Digital

  Glo Digital is one of Australia’s leading Drupal design & development
  agencies. Check out our mobile-friendly website http://glodigital.com.au/.
