<?php

/**
 * Implements hook_theme().
 */
function swinxyzoom_theme() {
  return array(
    'swinxyzoom_formatter' => array(
      'variables' => array('item' => NULL, 'image_style' => NULL, 'options' => array(), 'image_id' => NULL),
    ),
    'swinxyzoom_formatter_widget' => array(
      'variables' => array('item' => NULL, 'image_style' => NULL, 'options' => array(), 'image_id' => NULL),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function swinxyzoom_field_formatter_info() {
  $formatters = array(
    'swinxyzoom' => array(
      'label' => t('Swinxy Zoom'),
      'field types' => array('image'),
      'settings' => array(
        'image_style' => '',
        'large_image_style' => '',

        // JQuery settings
        'mode' => 'dock', // dock, window, lens and slippy
        'damping' => 8, // Int, False
        'steps' => 15, // Int
        'zoom' => 15, // Int
        'controls' => 1, // Bool
        'size' => '', // string - 'actual', 'src' or any valid CSS width
        'dock_position' => '', // top, right, bottom or left
        'lens_width' => '', // int
        'lens_height' => '', // int

        // Formatter settings
        'extras' => array('download' => 'download', 'link' => 'link'),

        'download' => array(
          'behaviour' => '',
          'size' => 1,
        ),
        'link' => array(
          'behaviour' => 'swinxyzoom-full-',
          'size' => 1,
        ),
      ),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function swinxyzoom_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $image_styles = image_style_options(FALSE);
  $element['image_style'] = array(
    '#title' => t('Image style'),
    '#type' => 'select',
    '#default_value' => $settings['image_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
  );
  $element['large_image_style'] = array(
    '#title' => t('Zoomed In Image style'),
    '#type' => 'select',
    '#default_value' => $settings['large_image_style'],
    '#empty_option' => t('None (original image)'),
    '#options' => $image_styles,
  );

  // JQuery settings
  $element['mode'] = array(
    '#title' => t('Mode'),
    '#type' => 'select',
    '#default_value' => $settings['mode'],
    '#options' => array(
      'dock' => t('Dock'),
      'window' => t('Window'),
      'lens' => t('Lens'),
      'slippy' => t('Slippy'),
    ),
    '#required' => TRUE,
    '#description' => t('See !link for documentation of this and other settings.',
        array('!link' => l(t(), 'http://www.swinxyapps.com/pages/apps/swinxy-zoom/index.html', array('external' => 1, 'attributes' => array('target' => '_blank'))))),
  );
  $element['damping'] = array(
    '#title' => t('damping'),
    '#type' => 'textfield',
    '#default_value' => $settings['damping'],
    '#description' => t('Interger, use 0 to disable.'),
  );
  $element['steps'] = array(
    '#title' => t('steps'),
    '#type' => 'textfield',
    '#default_value' => $settings['steps'],
  );
  $element['zoom'] = array(
    '#title' => t('zoom'),
    '#type' => 'textfield',
    '#default_value' => $settings['zoom'],
  );
  $element['controls'] = array(
    '#title' => t('controls'),
    '#type' => 'checkbox',
    '#default_value' => $settings['controls'],
  );
  $element['size'] = array(
    '#title' => t('width of the preview image'),
    '#type' => 'textfield',
    '#default_value' => $settings['size'],
    '#description' => t("You can use 'actual', 'src' or any valid CSS width."),
  );
  $element['dock_position'] = array(
    '#title' => t('Dock position'),
    '#type' => 'select',
    '#default_value' => $settings['dock_position'],
    '#options' => array(
      'top' => t('top'),
      'right' => t('right'),
      'bottom' => t('bottom'),
      'left' => t('left'),
    ),
    '#empty_option' => t('Use default'),
  );
  $element['lens_width'] = array(
    '#title' => t('lens width'),
    '#type' => 'textfield',
    '#default_value' => $settings['lens_width'],
  );
  $element['lens_height'] = array(
    '#title' => t('lens height'),
    '#type' => 'textfield',
    '#default_value' => $settings['lens_height'],
  );

  $element['extras'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Extras'),
    '#options' => array(
      'download' => t('Download link'),
      //'link' => t('Additional link'),
    ),
    '#default_value' => $settings['extras'],
  );

  // On hold till awaiting response to see if a full page version is possible.
  // @see https://bitbucket.org/ShadowShade/swinxyzoom/issue/4/full-page-mode
  $element['link'] = array(
    '#type' => 'fieldset',
    '#title' => t('Additional link'),
    '#access' => FALSE,
  );
  $swinxyzoom_options = array('swinxyzoom-full-' => t('Full page Swinxy Zoom of original image'));
  foreach ($image_styles as $style => $label) {
    // Do not santize as this is a select list.
    $swinxyzoom_options['swinxyzoom-full-' . $style] = t('Full page Swinxy Zoom of !label', array('!label' => $label));
  }
  $element['link']['behaviour'] = array(
    '#title' => t('Link behaviour'),
    '#type' => 'select',
    '#default_value' => $settings['link']['behaviour'],
    '#empty_option' => t('Hide'),
    '#options' => $swinxyzoom_options,
  );
  $element['link']['size'] = array(
    '#title' => t('Show size'),
    '#type' => 'checkbox',
    '#default_value' => $settings['link']['size'],
  );

  $element['download'] = array(
    '#type' => 'fieldset',
    '#title' => t('Download link'),
  );
  $element['download']['behaviour'] = array(
    '#title' => t('Style'),
    '#type' => 'select',
    '#default_value' => $settings['link']['behaviour'],
    '#empty_option' => t('Original image'),
    '#options' => $image_styles,
  );
  $element['download']['size'] = array(
    '#title' => t('Show size'),
    '#type' => 'checkbox',
    '#default_value' => $settings['download']['size'],
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function swinxyzoom_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = array();

  $image_styles = image_style_options(FALSE);
  // Unset possible 'No defined styles' option.
  unset($image_styles['']);
  // Styles could be lost because of enabled/disabled modules that defines
  // their styles in code.
  if (!empty($image_styles[$settings['image_style']])) {
    $summary[] = t('Preview Image: @style', array('@style' => $image_styles[$settings['image_style']]));
  }
  else {
    $summary[] = t('Preview Image: Original image');
  }
  if (!empty($image_styles[$settings['large_image_style']])) {
    $summary[] = t('Main Image: @style', array('@style' => $image_styles[$settings['large_image_style']]));
  }
  else {
    $summary[] = t('Main Image: Original image');
  }

  if (empty($settings['extras']['download'])) {
    $summary[] = t('Download: hidden');
  }
  else {
    $summary[] = t('Download: @style', array('@style' => empty($settings['download']['style']) ? t('Original') : $image_styles[$settings['download']['style']]));
  }

  /*
  if (empty($settings['extras']['link'])) {
    $summary[] = t('Behaviour: none');
  }
  else {
    $summary[] = t('Behaviour: @behaviour', array('@behaviour' => $settings['link']['behaviour']));
  }
  */
  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function swinxyzoom_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  list($id) = entity_extract_ids($entity_type, $entity);
  $settings = $display['settings'];

  $path = drupal_get_path('module', 'swinxyzoom');
  $ids = array();
  foreach ($items as $delta => $item) {
    $image_id = $entity_type . '-' . $id . '-' . $delta;
    $element[$delta] = array(
      '#theme' => 'swinxyzoom_formatter',
      '#item' => $item,
      '#image_id' => $image_id,
      '#image_style' => $settings['image_style'],
      '#options' => $settings,
    );
    $js_options = array();
    $str_settings = array('mode', 'size');
    foreach ($str_settings as $key) {
      if (isset($settings[$key]) && strlen($settings[$key])) {
        $js_options[$key] = $settings[$key];
      }
    }
    $int_settings = array('steps', 'zoom');
    foreach ($int_settings as $key) {
      if (isset($settings[$key]) && strlen($settings[$key])) {
        $js_options[$key] = (int)$settings[$key];
      }
    }
    $js_options['controls'] = isset($settings[$key]) ? (bool) $settings[$key] : TRUE;
    if (isset($settings['damping']) && strlen($settings['damping'])) {
      if (empty($settings['damping'])) {
        $js_options['damping'] = FALSE;
      }
      else {
        $js_options['damping'] = (int) $settings['damping'];
      }
    }
    if (!empty($settings['dock_position'])) {
      $js_options['dock']['position'] = $settings['dock_position'];
    }

    if (!empty($settings['lens_width'])) {
      $js_options['lens']['width'] = (int) $settings['lens_width'];
    }
    if (!empty($settings['lens_height'])) {
      $js_options['lens']['height'] = (int) $settings['lens_height'];
    }

    $ids[$image_id] = $js_options;
  }
  if ($ids) {
    $info = libraries_load('swinxyzoom');
    if ($info['installed']) {
      $element['#attached']['js'][] = $path . '/js/drupal.swinxyzoom.js';
      $element['#attached']['js'][] = array(
        'data' => array(
          'swinxyzoom' => array('targets' => $ids),
        ),
        'type' => 'setting',
      );
      $element['#attached']['css'][] = $path . '/css/swinxyzoom.css';
    }
  }
  return $element;
}

function theme_swinxyzoom_formatter_widget($variables) {
  $item = $variables['item'];
  $options = $variables['options'];
  $image = array(
    'path' => $item['uri'],
  );

  if (array_key_exists('alt', $item)) {
    $image['alt'] = $item['alt'];
  }

  if (isset($item['attributes'])) {
    $image['attributes'] = $item['attributes'];
  }

  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

  // Do not output an empty 'title' attribute.
  if (isset($item['title']) && drupal_strlen($item['title']) > 0) {
    $image['title'] = $item['title'];
  }
  $image['attributes']['id'] = $variables['image_id'];

  // The image rendered onto the page (preview or small image)
  if ($variables['image_style']) {
    $image['style_name'] = $variables['image_style'];
    $output = theme('image_style', $image);
  }
  else {
    $output = theme('image', $image);
  }
  $anchor_id = $image['attributes']['id'] . '-anchor';

  // The linked image that is the large or zoomed in image.
  // large_image_style
  if (empty($options['large_image_style'])) {
    $full_src = file_create_url($image['path']);
  }
  else {
    $full_url = image_style_url($options['large_image_style'], $image['path']);
    $full_src = file_create_url($full_url);
  }


  $output = '<a href="' . $full_src . '" id="' . $anchor_id . '" class="sxyzoom swinxywindow">' . $output . '</a>';
  return $output;
}

/**
 * Returns HTML for an image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: Associative array of image data, which may include "uri", "alt",
 *     "width", "height", "title" and "attributes".
 *   - image_style: An optional image style.
 *
 * @ingroup themeable
 */
function theme_swinxyzoom_formatter($variables) {
  $options = $variables['options'];
  $item = $variables['item'];

  $output = '<div class="swinxy-zoom-item">';
  $output .= theme('swinxyzoom_formatter_widget', $variables);

  $output = '<div class="swinxyzoom-box-wrapper' . (empty($options['controls']) ? '' : ' swinxyzoom-controls') . '"><div class="swinxyzoom-box">' . $output . '</div></div>';
  $links = array();
  if (!empty($options['extras']['link'])) {
    list($type, $subtype) = explode('-', $options['link']['behaviour']);
    $func = 'swinxyzoom_' . $type . '_' . $subtype . '_link_behaviour';
    if (function_exists($func)) {
      if ($link = $func($item, $options['link'], $variables)) {
        $links[] = '<span class="view-additional">' . $link . '</span> ';
      }
    }
  }
  if (!empty($options['extras']['download'])) {
    $show_size = !empty($options['download']['size']);
    if ($show_size && $info = image_get_info($item['uri'])) {
      $size = empty($info['file_size']) ? 0 : format_size($info['file_size']);
    }
    $download_link = '';
    if (empty($options['download']['behaviour'])) {
      $img_url = file_create_url($item['uri']);
    }
    else {
      $path = image_style_path('square_thumbnail', $item['uri']);
      $img_url = image_style_url('square_thumbnail', $path);
    }
    $download_link = l($size ? t('View image (@size)', array('@size' => $size)) : t('View image'), $img_url);
    $links[] = '<span class="view-image">' . $download_link . '</span> ';
  }
  if ($links) {
    $output .= '<div class="additional-links">' . implode(' | ', $links) . '</div>';
  }
  $output .= '</div>';
  return $output;
}

// Planned Full page support. Currently doesn't render very nicely in a full
// page.

/**
 * Implements hook_menu().
function swinxyzoom_menu() {
  $items['swinxyzoom/%file'] = array(
    'title' => 'Swinxy Zoom',
    'page callback' => 'swinxyzoom_page',
    'page arguments' => array(1),
    'access callback' => 'file_entity_access',
    'access arguments' => array('view', 1),
    'type' => MENU_CALLBACK,
  );
  $items['swinxyzoom/%file/%'] = array(
    'title' => 'Swinxy Zoom',
    'page callback' => 'swinxyzoom_page',
    'page arguments' => array(1, 2),
    'access callback' => 'file_entity_access',
    'access arguments' => array('view', 1),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

function swinxyzoom_page($file, $style = '') {
  $info = libraries_load('swinxyzoom');
  if ($info['installed']) {
    $path = drupal_get_path('module', 'swinxyzoom');
    $build['#attached']['js'][] = $path . '/js/drupal.swinxyzoom.js';
    $build['#attached']['js'][] = array(
      'data' => array(
        'swinxyzoom' => array('targets' => array(
            'swinxy-zoom-file-' . $file->fid => array(
              'mode' => 'window',
              'size' => 'actual',
              'controls' => TRUE,
            ),
          ),
        ),
      ),
      'type' => 'setting',
    );
    $build['#attached']['css'][] = $path . '/css/swinxyzoom.css';
  }

  $build['image'] = array(
    '#theme' => 'swinxyzoom_formatter',
    '#item' => (array) $file,
    '#image_id' => 'swinxy-zoom-file-' . $file->fid,
    '#image_style' => $style,
  );
  return $build;
}

function swinxyzoom_swinxyzoom_full_link_behaviour($item, $options, $variables) {
  $show_size = !empty($options['size']);
  $size = 0;
  list(,, $style) = explode('-', $options['behaviour']);
  if ($style) {
    $path = image_style_path($style, $item['uri']);
    if ($show_size && $info = image_get_info($path)) {
      $size = empty($info['file_size']) ? 0 : format_size($info['file_size']);
    }
  }
  else {
    if ($show_size && $info = image_get_info($item['uri'])) {
      $size = empty($info['file_size']) ? 0 : format_size($info['file_size']);
    }
  }
  return l($size ? t('Full page (@size)', array('@size' => $size)) : t('Full page'), 'swinxyzoom/' . $item['fid'] . ($style ? '/' . $style : ''));
}
 */


/**
 * Implements hook_libraries_info().
 */
function swinxyzoom_libraries_info() {
  $libraries['swinxyzoom'] = array(
    'name' => 'SwinxyZoom jQuery Plugin',
    'vendor url' => 'http://www.swinxyapps.com/pages/apps/swinxy-zoom/index.html',
    'download url' => 'http://www.swinxyapps.com/pages/apps/swinxy-zoom/index.html',
    'version callback' => 'swinxyzoom_library_version',
    'version arguments' => array(
      'file' => 'package.json',
    ),
    'files' => array(
      'js' => array(
        'libs/jquery.mousewheel.js',
        'dist/jquery.swinxy-combined.min.js',
      ),
      'css' => array(
        'dist/jquery.swinxy-combined.css'
      ),
    ),
  );
  return $libraries;
}

/**
 * Library module to provide the callback for the version number.
 */
function swinxyzoom_library_version($info, $arguments) {
  if (isset($info['library path'])) {
    $file_path = DRUPAL_ROOT . '/' . $info['library path'] . '/' . $arguments['file'];
    if (file_exists($file_path)) {
      if ($string = file_get_contents($file_path)) {
        $json_a = json_decode($string, TRUE);
        return isset($json_a['version']) ? $json_a['version'] : '0.0.1';
      }
    }
  }
}
