(function ($) {

  Drupal.behaviors.swinxyZoom = {
    attach: function (context) {
      if (Drupal.settings.swinxyzoom) {
        for (var target_id in Drupal.settings.swinxyzoom.targets) {
          console.log(target_id);
          if (Drupal.settings.swinxyzoom.is_page) {
          }
          var op = $.extend({}, Drupal.settings.swinxyzoom.targets[target_id]);
          $('#' + target_id + '-anchor').swinxyzoom(op);
        }
      }
    }
  };

})(jQuery);

